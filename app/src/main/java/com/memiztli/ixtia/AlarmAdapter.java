package com.memiztli.ixtia;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by martin on 24/02/18.
 */

public class AlarmAdapter extends ArrayAdapter<Alarm> implements View.OnClickListener{
    private ArrayList<Alarm> dataSet;
    Context mContext;
    boolean gps_enabled = false;

    AlarmDB alarmDB;



    // View lookup cache
    private static class ViewHolder {
        TextView tv_alarm_name;
        TextView tv_alarm_last_active;
        ImageView img_alarm_active;
    }

    public AlarmAdapter(ArrayList<Alarm> data, Context context) {

        super(context, R.layout.alarm_item, data);
        this.dataSet = data;
        this.mContext=context;


    }

    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        Alarm dataModel=(Alarm) object;

        switch (v.getId())
        {
            case R.id.img_alarm_active:

                alarmDB = new AlarmDB(v.getContext());

                //RESTABLECER LA ULTIMA ALARMA
                AlarmSettings alarmSettings = alarmDB.getAlarmSettings();
                alarmSettings.setLast_alarm_id(0);
                alarmDB.setAlarmSettings(alarmSettings);


                Alarm alarm = new Alarm();
                alarm = alarmDB.getAlarmById(dataModel.getId());
                ImageView imageView = (ImageView) v;
                if (alarm.isActive()){

                    imageView.setImageResource(R.drawable.ic_alarm_off_white);
                    imageView.setBackgroundResource(R.drawable.circle_red);
                    v = imageView;

                    alarm.setActive(false);
                    alarm.setLast_active("");
                    alarmDB.modifyAlarm(alarm);
                    Snackbar.make(v, "Alarma '" +dataModel.getName().toUpperCase() + "' Desactivada", Snackbar.LENGTH_LONG)
                            .setAction("No action", null).show();
                    alarmDB.setAlarmSettings(new AlarmSettings());
                }
                else {

                    imageView.setImageResource(R.drawable.ic_alarm_on_white);
                    imageView.setBackgroundResource(R.drawable.circle_primary_dark);
                    v = imageView;

                    alarm.setActive(true);
                    alarm.setLast_active("");
                    alarmDB.modifyAlarm(alarm);
                    Snackbar.make(v, "Alarma '" + dataModel.getName().toUpperCase() + "' Activada", Snackbar.LENGTH_LONG)
                            .setAction("No action", null).show();
                    alarmDB.setAlarmSettings(new AlarmSettings());

                }

                break;


        }
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Alarm dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.alarm_item, parent, false);

            viewHolder.tv_alarm_name = (TextView) convertView.findViewById(R.id.tv_alarm_name);

            viewHolder.img_alarm_active = (ImageView) convertView.findViewById(R.id.img_alarm_active);
            viewHolder.tv_alarm_last_active = (TextView) convertView.findViewById(R.id.tv_alarm_last_active);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        /*Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.layout.up_from_bottom : R.layout.down_from_top);
        result.startAnimation(animation);*/


        viewHolder.tv_alarm_name.setText(dataModel.getName());

        viewHolder.tv_alarm_last_active.setText("Ultima vez: "+dataModel.getLast_active());



        if (dataModel.isActive()){
            viewHolder.img_alarm_active.setImageResource(R.drawable.ic_alarm_on_white);
            viewHolder.img_alarm_active.setBackgroundResource(R.drawable.circle_primary_dark);
        }
        else{
            viewHolder.img_alarm_active.setImageResource(R.drawable.ic_alarm_off_white);
            viewHolder.img_alarm_active.setBackgroundResource(R.drawable.circle_red);

        }


        viewHolder.img_alarm_active.setOnClickListener(this);
        viewHolder.img_alarm_active.setTag(position);
        // Return the completed view to render on screen
        return convertView;
    }
}
