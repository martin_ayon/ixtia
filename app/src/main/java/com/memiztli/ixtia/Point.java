package com.memiztli.ixtia;

/**
 * Created by martin on 24/02/18.
 */

public class Point {

    Double latitude;
    Double longitude;

    public Point(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Point() {
        this.latitude = 0.0;
        this.longitude =0.0;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     *
     *
     * @param point com.memiztli.ixtia.Point
     * @return Double in meters
     */
    public Double getDistance(Point point){

        double eathRadius = 6371;
        double dLat = Math.toRadians(point.getLatitude() - this.getLatitude());
        double dLng = Math.toRadians(point.getLongitude() - this.getLongitude());
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(this.getLatitude())) * Math.cos(Math.toRadians(point.getLatitude()));
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));
        double distancia = eathRadius * va2;


        //RETORNA EL
        return Double.valueOf(Math.round(distancia * 1000));

    }
}
