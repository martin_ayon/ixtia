package com.memiztli.ixtia;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    CardView card_alarms,card_info, card_settings;
    Intent intent_ixtia_service;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION  = 100;
    ShowcaseView showcaseView= null;
    int contador = 0;
    FloatingActionButton fab;
    private MediaPlayer mediaPlayer = null;

    private Target t_card_alarmas,t_card_settings,t_card_info, t_btn_add_alarm;

    boolean isFirstTime = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


        //VALIDAR SI ES LA PIMERA VEZ QUE SE ABRE LA APP
        isFirstTime = getSharedPreferences("PREFERENCE",MODE_PRIVATE).getBoolean("isFirstMain",true);


        //RESETEAR ULTIMO ID DE ALARMA EN DB
        this.resetLastAlarmaId();

        if (
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, MY_PERMISSIONS_REQUEST_LOCATION);


        } else {
            intent_ixtia_service = new Intent(this, IxtiaService.class);
            startService(intent_ixtia_service);
        }

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent;
                intent = new Intent(view.getContext(),AddAlarm.class);
                startActivity(intent);
            }
        });

        card_alarms = (CardView) this.findViewById(R.id.card_viewalarms);
        card_alarms.setOnClickListener(this);

        card_info = (CardView) this.findViewById(R.id.card_credits);
        card_info.setOnClickListener(this);

        card_settings  = (CardView) this.findViewById(R.id.card_settings);
        card_settings.setOnClickListener(this);

        MobileAds.initialize(this, "ca-app-pub-2113453459727687~6753923264");


        AdView  mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


        //TARGEST SHOW CASE VIEW
        t_card_alarmas = new ViewTarget(R.id.card_viewalarms,this);
        t_card_info = new ViewTarget(R.id.card_credits,this);
        t_card_settings = new ViewTarget(R.id.card_settings,this);
        t_btn_add_alarm = new ViewTarget(R.id.fab,this);

        if (isFirstTime) {

            showcaseView = new ShowcaseView.Builder(this)
                    .setTarget(Target.NONE)
                    .setOnClickListener(this)
                    .setContentTitle("Ixtia")
                    .setContentText("Bienvenido a Ixtia")
                    .setStyle(R.style.StyleShowCase)
                    .build();
            showcaseView.setButtonText("Siguiente");
            //showcaseView.setBackgroundColor(Color.BLACK);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent settings = new Intent(this,SettingsActivity.class);
            startActivity(settings);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        if (isFirstTime){

            switch (contador){
                case 0: showcaseView.setShowcase(t_card_alarmas,true);
                    showcaseView.setContentTitle("Ver Alarmas");
                    showcaseView.setContentText("Aqu\u00ED ve tus alarmas");
                    break;
                case 1: showcaseView.setShowcase(t_card_info,true);
                    showcaseView.setContentTitle("Acerca de");
                    showcaseView.setContentText("Informaci\u00F3n de la aplicaci\u00F3n");
                    break;
                case 2: showcaseView.setShowcase(t_card_settings,true);
                    showcaseView.setContentTitle("Configuraci\u00F3n");
                    showcaseView.setContentText("Configuraci\u00F3n de la aplicaci\u00F3n");
                    break;
                case 3: showcaseView.setShowcase(t_btn_add_alarm,true);
                    showcaseView.setContentTitle("Agregar Alarmas");
                    showcaseView.setContentText("Agrega una alarma aqu\u00ED");
                    showcaseView.setButtonText("Cerrar");
                    break;
                case  4:
                    showcaseView.hide();
                    getSharedPreferences("PREFERENCE",MODE_PRIVATE).edit().putBoolean("isFirstMain",false).commit();
                    break;
            }

            contador ++;
        }



        if (v.getId() == this.card_alarms.getId()){
            Intent intent;
            intent = new Intent(this,ViewAlarmsActivity.class);
            startActivity(intent);
        }

        else if (v.getId() == card_info.getId()){
            Intent intent;
            intent = new Intent(this,AcercaActivity.class);
            startActivity(intent);

        }

        else if (v.getId() == card_settings.getId()){
            Intent intent;
            intent = new Intent(this,SettingsActivity.class);
            startActivity(intent);
        }


    }

    @Override
    protected void onDestroy() {
        stopService(intent_ixtia_service);
        super.onDestroy();

    }


    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_gps_fixed_black)
                .setTitle("Cerrar Alarma")
                .setMessage("No podre avisarte si me cierras\uD83D\uDE25 \u00BFDeseas cerrar de todos modos?")
                .setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //Stop the activity
                        finish();
                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        moveTaskToBack(true);
                    }

                })
                .show();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //Handle the back button
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            //Ask the user if they want to quit
            new AlertDialog.Builder(this)
                    .setIcon(R.drawable.ic_gps_fixed_black)
                    .setTitle("Cerrar Alarma")
                    .setMessage("No podre avisarte si me cierras\uD83D\uDE25 \u00BFDeseas cerrar de todos modos?")
                    .setCancelable(false)
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Stop the activity
                            finish();
                        }

                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //Stop the activity
                           moveTaskToBack(true);
                        }

                    })
                    .show();

            return true;
        }
        else {
            return super.onKeyDown(keyCode, event);

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    intent_ixtia_service = new Intent(this, IxtiaService.class);
                    startService(intent_ixtia_service);


                } else {

                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, MY_PERMISSIONS_REQUEST_LOCATION);

                }
                return;
            }

        }
    }


    private void resetLastAlarmaId(){

        AlarmDB alarmDB = new AlarmDB(this);


        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();

        String date_last_active = alarmDB.getAlarmSettings().getLast_alarm_active();
        String date_now = format.format(calendar.getTime());

        Date d1 = null;
        Date d2 = null;

        try {

            if (!date_last_active.isEmpty())
            {
                d1 = format.parse(date_last_active);
                d2 = format.parse(date_now);

                calendar.setTime(d1);
                int last_day = calendar.get(Calendar.DAY_OF_MONTH);
                int last_month = calendar.get(Calendar.MONTH);

                calendar.setTime(d2);
                int actual_day = calendar.get(Calendar.DAY_OF_MONTH);
                int actual_month = calendar.get(Calendar.MONTH);

                if (actual_day != last_day || last_month != actual_month){

                    AlarmSettings alarmSettings = alarmDB.getAlarmSettings();
                    alarmSettings.setLast_alarm_id(0);
                    alarmDB.setAlarmSettings(alarmSettings);

                    System.out.println("Reset Alarma Settings");


                }
                else {
                    System.out.println("Not Reset Alarma Settings");

                }
            }

        } catch (Exception e) {
            System.out.println("Datatime: ERROR" + e.getMessage());

        }
    }

}
