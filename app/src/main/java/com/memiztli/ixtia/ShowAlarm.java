package com.memiztli.ixtia;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.speech.tts.Voice;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

public class ShowAlarm extends AppCompatActivity implements View.OnClickListener,TextToSpeech.OnInitListener{

    TextView tv_alarm_name;
    Button btn_stop;
    Alarm alarm;
    AlarmDB alarmDB;

    TextToSpeech textToSpeech;
    boolean stop_alarm = false;
    Vibrator vibrator ;
    private AudioManager audioManager = null;
    RingtoneManager ringtoneManager;
    Ringtone ringtone;
    int current_vol = 0;
    boolean is_playing_music = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_alarm);

        int id = this.getIntent().getIntExtra("id",0);

        //INICIAR EL AUDIO MANAGER
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        current_vol = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);


        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);


        alarmDB = new AlarmDB(this);



        alarm = new Alarm();
        if (id != 0){
            alarm = alarmDB.getAlarmById(id);
        }

        tv_alarm_name = (TextView) findViewById(R.id.tv_show_alarm_name);
        tv_alarm_name.setText(alarm.getName());
        btn_stop = (Button) findViewById(R.id.btn_show_alarm_stop);
        btn_stop.setOnClickListener(this);

        MobileAds.initialize(this, "ca-app-pub-2113453459727687~6753923264");


        AdView mAdView = findViewById(R.id.adViewShowAlarm);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


        //PAUSAR MUSICA PARA ACTIVAR ALARMA
        is_playing_music = audioManager.isMusicActive();
        this.playPauseMusic();


        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        boolean notifications_voice_modePref = sharedPref.getBoolean("notifications_voice_mode", true);

        boolean notifications_new_message_vibratePref = sharedPref.getBoolean("notifications_new_message_vibrate", true);

        boolean notifications_ringtone_modePref = sharedPref.getBoolean("notifications_ringtone_mode", true);;



        if (notifications_new_message_vibratePref){
            long[] pattern = {0,100,500,100,200,500,100,300,100};
            vibrator.vibrate(pattern,6);
        }

        if (notifications_ringtone_modePref){

            String alarm = sharedPref.getString("notifications_ringtone","");

            if (!alarm.isEmpty()){

                //SET VOL 10 TO TEXT TO SPEAK
                audioManager.setStreamVolume(AudioManager.STREAM_RING, audioManager.getStreamMaxVolume(AudioManager.STREAM_RING) - 2,0);
                Uri alarm_uri = Uri.parse(alarm);
                playRingtone(this.getApplicationContext(),alarm_uri);
            }


        }

        if (notifications_voice_modePref){

            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)-2,0);
            textToSpeech = new TextToSpeech(this, this);

        }



    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btn_show_alarm_stop){
            this.stop_alarm = true;
            vibrator.cancel();
            if (textToSpeech != null)
                textToSpeech.stop();

            this.stopRingtoone();
            this.playPauseMusic();

            //COLOCAR EL VOLUMEN QUE TENIA
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,current_vol,0);

            this.finish();
        }

    }

    @Override
    public void onBackPressed() {

        vibrator.cancel();
        if (textToSpeech != null)
            textToSpeech.stop();

        this.stopRingtoone();
        this.playPauseMusic();

        //COLOCAR EL VOLUMEN QUE TENIA
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,current_vol,0);

        finish();
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            String text = " " + getString(R.string.alarm_detected_alert_text) + alarm.getName()
                    + ", " + getString(R.string.alarm_detected_alert_text) + alarm.getName()
                    + ", " + getString(R.string.alarm_detected_alert_text) + alarm.getName()
                    + ", " + getString(R.string.alarm_detected_alert_text) + alarm.getName()
                    + ", " + getString(R.string.alarm_detected_alert_text) + alarm.getName();

            textToSpeech.speak(text, textToSpeech.QUEUE_FLUSH, null);


        } else {

            Toast.makeText(getApplicationContext(), "No soporta Text to speak", Toast.LENGTH_LONG);
        }
    }

    private void playRingtone(Context context, Uri ringtone_uri){

        ringtone = RingtoneManager.getRingtone(context, ringtone_uri);
        ringtone.play();

    }

    private void stopRingtoone(){
        if (ringtone != null){
            if (ringtone.isPlaying()){
                ringtone.stop();
            }
        }
    }

    private void playPauseMusic(){

        //PAUSE ANY MUISC PLAYING
        if (is_playing_music && audioManager != null){

            long eventtime = SystemClock.uptimeMillis();
            KeyEvent downEvent = new KeyEvent(eventtime, eventtime, KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE, 0);

            //PAUSE MUSIC PLAYER
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                audioManager.dispatchMediaKeyEvent(downEvent);
                eventtime++;
                KeyEvent upEvent = new KeyEvent(eventtime,eventtime,KeyEvent.ACTION_UP,KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE, 0);
                audioManager.dispatchMediaKeyEvent(upEvent);
            }

        }

    }



}
