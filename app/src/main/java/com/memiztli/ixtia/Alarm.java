package com.memiztli.ixtia;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by martin on 24/02/18.
 */

public class Alarm {
    int id;
    String  name;
    Double latitude;
    Double longitude;
    double range;
    boolean active;
    String last_active;

    public static final int MIN_RANGE = 300;

    public Alarm(int id, String name, Double latitude, Double longitude, double range, boolean active, String last_active) {
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.range = range;
        this.active = active;
        this.last_active = last_active;
    }

    public Alarm() {
        this.id = 0;
        this.name = "";
        this.latitude = 0.0;
        this.longitude = 0.0;
        this.range = 300;
        this.active = false;
        this.last_active = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public double getRange() {
        return range;
    }

    public void setRange(double range) {
        this.range = range;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getLast_active() {
        return last_active;
    }

    public void setLast_active(String last_active) {
        this.last_active = last_active;
    }

    /**
     * Regresa un objeto de la case Point con la latitud y longitud
     * @return Point
     */
    public  Point getlocation(){
        return new Point(this.latitude,this.longitude);
    }

    /**
     * RETORNA EN MINUTOS LA ULTIMA VEZ QUE SE ACTIVO LA ALARMA
     * @return double
     */
    public double getLastActiveInMinutes(){
        double minutes =0;

        if (this.last_active.isEmpty()){
            return 11;
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar c = Calendar.getInstance();

        String now= format.format(c.getTime());

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(this.getLast_active());
            d2 = format.parse(now);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();
            return TimeUnit.MILLISECONDS.toMinutes(diff);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return minutes;
    }
}
