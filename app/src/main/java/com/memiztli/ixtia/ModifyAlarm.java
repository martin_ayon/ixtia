package com.memiztli.ixtia;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;

import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.res.ResourcesCompat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class ModifyAlarm extends AppCompatActivity implements GoogleMap.OnMapLongClickListener,OnMapReadyCallback, SeekBar.OnSeekBarChangeListener {

    GoogleMap map;
    EditText et_name;
    TextView tv_range;
    SeekBar sbar_range;
    Marker marker = null;
    Circle circle = null;
    Switch switchActive;
    Alarm alarm;
    AlarmDB alarmDB ;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 100;


    final int color_purple = Color.argb(50,213, 0, 249);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_alarm);

        alarmDB = new AlarmDB(this);
        int id = 0;
        id = this.getIntent().getIntExtra("id",0);
        alarm = alarmDB.getAlarmById(id);



        et_name = (EditText) findViewById(R.id.et_name_mod);
        tv_range = (TextView) findViewById(R.id.tv_range_mod);
        sbar_range = (SeekBar) findViewById(R.id.sbar_range_mod);
        sbar_range.setOnSeekBarChangeListener(this);

        switchActive = (Switch) findViewById(R.id.sw_active_mod);
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_mod);


        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar_mod);
        setSupportActionBar(myToolbar);

        mapFragment.getMapAsync(this);


        et_name.setText(alarm.getName());

        sbar_range.setProgress((int) alarm.getRange() - Alarm.MIN_RANGE);
        sbar_range.incrementProgressBy(10);
        tv_range.setText("Rango (" + (alarm.getRange() ) + " M )");

        switchActive.setChecked(alarm.isActive());



    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {


        if(map != null){

                progress = progress / 10;
                progress = progress * 10;

                tv_range.setText("Rango (" + (progress + Alarm.MIN_RANGE) + " M )");

                seekBar.setProgress(progress);
                if (marker != null) {


                    CircleOptions circleOptions = new CircleOptions();
                    circleOptions.center(marker.getPosition());
                    circleOptions.radius(progress + Alarm.MIN_RANGE);
                    circleOptions.strokeColor(getResources().getColor(R.color.colorPrimaryDark));
                    circleOptions.fillColor(color_purple);
                    circleOptions.strokeWidth(2);


                    if (circle != null) {
                        circle.remove();
                    }

                    circle = map.addCircle(circleOptions);

                }

        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        if (marker != null)
            marker.remove();

        marker = map.addMarker(new MarkerOptions().position(latLng).title("Localizacion"));

        if (circle != null){
            circle.remove();
        }

        CircleOptions circleOptions = new CircleOptions();
        circleOptions.center(marker.getPosition());
        circleOptions.radius(sbar_range.getProgress() + Alarm.MIN_RANGE);
        circleOptions.strokeColor( getResources().getColor(R.color.colorPrimaryDark));
        circleOptions.fillColor(color_purple);
        circleOptions.strokeWidth(2);

        circle = map.addCircle(circleOptions);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {


        this.map = googleMap;
        this.map.setOnMapLongClickListener(this);

        LatLng location = new LatLng(20.664520, -103.345991);

        map.moveCamera(CameraUpdateFactory.newLatLng(location));
        map.moveCamera(CameraUpdateFactory.zoomTo(12));

        this.addMarker(alarm);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, MY_PERMISSIONS_REQUEST_LOCATION);

        }
        else {
            map.setMyLocationEnabled(true);
        }

    }

    //METODO PARA CREAR EL MENU
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mod_alarm, menu);
        return true;
    }

    //METODO PARA EL LISTENER DEL MENU
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == R.id.action_modify_alarm) {
            if (marker == null){
                Snackbar snackbar = Snackbar
                        .make(this.et_name, "Manten presionado para seleccionar ubicacion", Snackbar.LENGTH_LONG);

                snackbar.show();
                return false;
            }
            else if (et_name.getText().toString().isEmpty()){
                et_name.setHint("Escribe un nombre");
                et_name.setHintTextColor(ResourcesCompat.getColor(getResources(), R.color.colorAccent, null));
                return false;
            }
            else {

                AlarmDB alarmDB = new AlarmDB(et_name.getContext());

                String name = et_name.getText().toString().trim();
                double latitude = this.marker.getPosition().latitude;
                double longitude = this.marker.getPosition().longitude;
                boolean isActive = this.switchActive.isChecked();
                double range = this.sbar_range.getProgress() + Alarm.MIN_RANGE;


                alarm.setName(name);
                alarm.setLatitude(latitude);
                alarm.setLongitude(longitude);
                alarm.setActive(isActive);
                alarm.setRange(range);

                if (alarmDB.modifyAlarm(alarm)){
                    this.setResult(Activity.RESULT_OK);
                    this.finish();
                }

                else {
                    setResult(Activity.RESULT_OK);
                    this.finish();
                }


            }
        }

        else  if (id == R.id.action_delete_alarm){

            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_delete)
                    .setTitle("Eliminar Alarma")
                    .setMessage("�Deseas Borrar esta Alarma?")
                    .setCancelable(false)
                    .setPositiveButton("Si", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            if (alarmDB.deleteAlarm(alarm)){

                                setResult(Activity.RESULT_OK);
                                finish();
                            }

                            else {
                                setResult(Activity.RESULT_OK);
                                finish();
                            }

                        }

                    })
                    .setNegativeButton("No", null)
                    .show();


        }

        return super.onOptionsItemSelected(item);
    }


    public void addMarker(Alarm alarm){
        LatLng latLng = new LatLng(alarm.getLatitude(),alarm.getLongitude());
        if (marker != null)
            marker.remove();

        marker = map.addMarker(new MarkerOptions().position(latLng).title("Localizacion"));

        if (circle != null){
            circle.remove();
        }

        CircleOptions circleOptions = new CircleOptions();
        circleOptions.center(marker.getPosition());
        circleOptions.radius(sbar_range.getProgress() + Alarm.MIN_RANGE);
        circleOptions.strokeColor( getResources().getColor(R.color.colorPrimaryDark));
        circleOptions.fillColor(color_purple);
        circleOptions.strokeWidth(2);

        circle = map.addCircle(circleOptions);



        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng)      // Sets the center of the map to Mountain View
                .zoom(14)                   // Sets the zoom
                .bearing(0)                // Sets the orientation of the camera to east
                .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                .build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                        return;
                    }
                    else
                        map.setMyLocationEnabled(true);

                } else {

                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, MY_PERMISSIONS_REQUEST_LOCATION);

                }
                return;
            }

        }
    }



}
