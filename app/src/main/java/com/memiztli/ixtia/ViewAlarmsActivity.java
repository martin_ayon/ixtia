package com.memiztli.ixtia;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


import java.util.ArrayList;

public class ViewAlarmsActivity extends AppCompatActivity {


    ListView list_view_alarms;
    ArrayList<Alarm> dataModels;
    private static AlarmAdapter adapter;
    AlarmDB alarmDB;
    int activity_modify = 1;
    int activity_create = 2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_alarms);

        list_view_alarms=(ListView)findViewById(R.id.listViewAlarms);
        alarmDB = new AlarmDB(this);

        this.loadAlarms();
    }


    private void loadAlarms(){

        list_view_alarms=(ListView)findViewById(R.id.listViewAlarms);

        dataModels= new ArrayList<>();
        dataModels = alarmDB.getAllAlarms();

        adapter= new AlarmAdapter(dataModels,getApplicationContext());
        list_view_alarms.setAdapter(adapter);


        list_view_alarms.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Alarm dataModel= dataModels.get(position);

                Intent modify_alarm = new Intent();
                modify_alarm = new Intent(view.getContext(),ModifyAlarm.class);
                modify_alarm.putExtra("id",dataModel.getId());
                startActivityForResult(modify_alarm, activity_modify);

            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_add_view);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent;
                intent = new Intent(view.getContext(),AddAlarm.class);
                startActivityForResult(intent,activity_create);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        if ((requestCode == activity_modify) && (resultCode == RESULT_OK)){
            //this.last_alarm_id = 0;
            this.loadAlarms();
        }

        else if ((requestCode == activity_create) && (resultCode == RESULT_OK)){
            this.loadAlarms();
        }

    }
}
