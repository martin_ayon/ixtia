package com.memiztli.ixtia;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by martin on 24/02/18.
 */

public class AlarmDB extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 4;
    public static final String DATABASE_NAME = "Ixtia.db";

    //COLUMNAS
    public static final String TABLE_NAME ="alarms";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String LATITUDE = "latitude";
    public static final String LONGTUDE = "longitude";
    public static final String RANGE = "range";
    public static final String ACTIVE = "active";
    public static final String LAST_ACTIVE = "last_active";


    public static final String TABLE_ALARMS_SETTINGS = "alarms_settings";
    public static final String LAST_ALARM_ID = "last_alarm_id";
    public static final String LAST_ALARM_ACTIVE = "last_alarm_active";



    public AlarmDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE " + TABLE_NAME + " ("
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + NAME + " TEXT NOT NULL,"
                + RANGE + " DOUBLE NOT NULL,"
                + LATITUDE + " DOUBLE NOT NULL,"
                + LONGTUDE + " DOUBLE NOT NULL,"
                + ACTIVE + " TINYINT NOT NULL,"
                + LAST_ACTIVE + " TEXT , "
                + " UNIQUE (" + ID + "))"
        );

        db.execSQL("CREATE TABLE " + TABLE_ALARMS_SETTINGS + " ( "
                + ID + " INTEGER PRIMARY KEY NOT NULL,"
                + LAST_ALARM_ID + " INTEGER ,"
                + LAST_ALARM_ACTIVE + " TEXT )"
        );

        db.execSQL("" +
                " INSERT INTO "
                + TABLE_ALARMS_SETTINGS
                + " (" + ID + " , " + LAST_ALARM_ID + " , " + LAST_ALARM_ACTIVE  + ")"
                + " VALUES (1,0,' ')");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ALARMS_SETTINGS);

        // Create tables again
        onCreate(db);

    }

    /**
     *  RETORNA TODAS LAS ALARMAS EN LA BASE DE DATOS
     * @return ArrayList<Alarm>
     */
    public ArrayList<Alarm> getAllAlarms(){
        ArrayList<Alarm>  alarms = new ArrayList<Alarm>();
        Cursor c = getReadableDatabase().query(TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null);

        if (c.moveToFirst()){
            do {
                Alarm alarm = new Alarm();
                alarm.setId(c.getInt(0));
                alarm.setName(c.getString(1));
                alarm.setRange(c.getDouble(2));
                alarm.setLatitude(c.getDouble(3));
                alarm.setLongitude(c.getDouble(4));
                boolean active = false;
                if (c.getInt(5)  != 0)
                    active = true;
                alarm.setActive(active);
                alarm.setLast_active(c.getString(6));
                alarms.add(alarm);

            }while (c.moveToNext());
        }
        c.close();
        return alarms;

    }

    /**
     *  RETORNA TODAS LAS ALARMAS EN LA BASE DE DATOS
     * @return ArrayList<Alarm>
     */
    public ArrayList<Alarm> getActiveAlarms(){
        ArrayList<Alarm>  alarms = new ArrayList<Alarm>();
        Cursor c = getReadableDatabase().query(TABLE_NAME,
                null,
                ACTIVE + " = ?", new String[]{String.valueOf(1)},
                null,
                null,
                null,
                null);

        if (c.moveToFirst()){
            do {
                Alarm alarm = new Alarm();
                alarm.setId(c.getInt(0));
                alarm.setName(c.getString(1));
                alarm.setRange(c.getDouble(2));
                alarm.setLatitude(c.getDouble(3));
                alarm.setLongitude(c.getDouble(4));
                boolean active = false;
                if (c.getInt(5)  != 0)
                    active = true;
                alarm.setActive(active);
                alarm.setLast_active(c.getString(6));
                alarms.add(alarm);

            }while (c.moveToNext());
        }
        c.close();
        return alarms;

    }

    /**
     * RETORNA UN OBJETO Alarm
     * Si no encuentra en la base de datos el id regresa un objeto Alarm vacio
     * @param id integer
     * @return Alarm
     */
    public Alarm getAlarmById(int id){
        Alarm alarm = new Alarm();
        Cursor c = getReadableDatabase().query(TABLE_NAME,
                null,
                ID + " = ?", new String[]{String.valueOf(id)},
                null,
                null,
                null,
                null);
        if (c.moveToFirst()){
            do {

                alarm.setId(c.getInt(0));
                alarm.setName(c.getString(1));
                alarm.setRange(c.getDouble(2));
                alarm.setLatitude(c.getDouble(3));
                alarm.setLongitude(c.getDouble(4));
                boolean active = false;
                if (c.getInt(5)  != 0)
                    active = true;
                alarm.setLast_active(c.getString(6));
                alarm.setActive(active);

            }while (c.moveToNext());
        }
        c.close();
        return alarm;
    }

    /**
     * Regresa true si se inserto el valor en la BD
     * @param alarm Alarm
     * @return boolean
     */
    public boolean addAlarm(Alarm alarm){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(NAME, alarm.getName() );
        values.put(RANGE, alarm.getRange());
        values.put(LATITUDE, alarm.getLatitude());
        values.put(LONGTUDE,alarm.getLongitude());
        values.put(ACTIVE, alarm.isActive());
        values.put(LAST_ACTIVE, "");

        if (db.insert(TABLE_NAME,null,values) > 0){
            db.close();
            return true;
        }

        db.close();

        return false;
    }

    /**
     *  Retorna true si se realizo la modificacion
     * @param alarm Alarm
     * @return boolean
     */
    public  boolean modifyAlarm(Alarm alarm){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(NAME, alarm.getName() );
        values.put(RANGE, alarm.getRange());
        values.put(LATITUDE, alarm.getLatitude());
        values.put(LONGTUDE,alarm.getLongitude());
        values.put(ACTIVE, alarm.isActive());

        if (alarm.getLast_active().isEmpty()){
            values.put(LAST_ACTIVE,alarm.getLast_active());
        }

        if (db.update(TABLE_NAME,values,
                ID + " =?", new String[]{String.valueOf(alarm.getId())}) > 0){
            db.close();
            return true;
        }

        db.close();
        return false;
    }

    /**
     * Regresa true si se elimino de la base  de datos
     * @param alarm Alarm
     * @return boolean
     */
    public boolean deleteAlarm(Alarm alarm){
        SQLiteDatabase db = this.getWritableDatabase();

        if(db.delete(TABLE_NAME, ID + " = ?",
                new String[] { String.valueOf(alarm.getId()) }) > 0){
            db.close();
            return true;
        }
        db.close();

        return false;
    }

    /**
     * Coloca la ultima activacion en la base de datos
     * @param alarm Alarm
     * @return boolean
     */
    public boolean setLastActive(Alarm alarm){

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String last_active= df.format(c.getTime());

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(LAST_ACTIVE, last_active );

        if (db.update(TABLE_NAME,values,
                ID + " =?", new String[]{String.valueOf(alarm.getId())}) > 0){
            db.close();
            return true;
        }
        db.close();

        return false;

    }



    /**
     * RETORNA UN OBJETO AlarmSettings
     * RETORNA LOS DATOS DE LA ULTIMA ALARMA ENCONTRADA
     * @return AlarmSettings
     */
    public AlarmSettings getAlarmSettings(){
        AlarmSettings alarmSettings = new AlarmSettings();
        Cursor c = getReadableDatabase().query(TABLE_ALARMS_SETTINGS,
                null,
                ID + " = ?", new String[]{String.valueOf(1)},
                null,
                null,
                null,
                null);
        if (c.moveToFirst()){
            do {


                alarmSettings.setId(c.getInt(0));
                alarmSettings.setLast_alarm_id(c.getInt(1));
                alarmSettings.setLast_alarm_active(c.getString(2));

            }while (c.moveToNext());
        }
        c.close();
        return alarmSettings;
    }

    /**
     * RETORNA UN OBJETO AlarmSettings
     * RETORNA LOS DATOS DE LA ULTIMA ALARMA ENCONTRADA
     * @return AlarmSettings
     */
    public boolean setAlarmSettings(AlarmSettings alarmSettings){

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String last_active= df.format(c.getTime());

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(LAST_ALARM_ID,  alarmSettings.getLast_alarm_id());
        values.put(LAST_ALARM_ACTIVE, last_active);

        if (db.update(TABLE_ALARMS_SETTINGS,values,
                ID + " =?", new String[]{String.valueOf(1)}) > 0){
            db.close();
            return true;
        }

        db.close();
        return false;
    }
}
