package com.memiztli.ixtia;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
//import android.util.Log;

import java.util.ArrayList;

import static android.location.LocationManager.*;
import static android.support.v4.app.ActivityCompat.startActivityForResult;

/**
 * Created by martin on 25/02/18.
 * IDEA TOMADA DE
 * https://stackoverflow.com/questions/28535703/best-way-to-get-user-gps-location-in-background-in-android
 */

public class IxtiaService extends Service {

    private static final String TAG = "IXTIAGPS";
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 0;
    private Intent intent_request_gps = null;
    private static final int ENABLE_GPS = 1;


    AlarmDB alarmDB;

    private class LocationListener implements android.location.LocationListener
    {
        Location mLastLocation;
        Intent intent_alarm = null;

        public LocationListener(String provider)
        {

            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);


        }

        @Override
        public void onLocationChanged(Location location)
        {

            try {
                //OBTENER INFORMACION DE LAS ALARMAS Y LAS CONFIGURACIONES
                ArrayList<Alarm> alarms = alarmDB.getActiveAlarms();
                AlarmSettings alarmSettings  = alarmDB.getAlarmSettings();


                Alarm alarm_near = new Alarm();
                Double min_distance = 0.0;
                Double distance = 0.0;


                //BUSCAR LA ALARMA ACTIVA CERCANA
                for (int i = 0; i < alarms.size(); i++){

                    Alarm alarm = alarms.get(i);

                        //VALIDAR QUE LA ALARMA NO ES LA ULTIMA QUE FUE ACTIVADA
                        // Y QUE HAYAN PASADO AL MENOS 10 MINUTOS DESPUES DE ACTIVARLA
                        if (alarm.getId() != alarmSettings.getLast_alarm_id() && alarm.getLastActiveInMinutes() > 10){

                            //VALIDAR SI LA DISTANCIA MINIMA ES LA PRIMERA
                            if (min_distance == 0.0){
                                min_distance = alarm.getlocation().getDistance(new Point(location.getLatitude(), location.getLongitude()));
                                alarm_near = alarm;
                            }

                            else {
                                //VALIDAR SI LA ALARMA TIENE MENOR DISTANCIA QUE LA ANTERIOR
                                Double distance_n = alarm.getlocation().getDistance(new Point(location.getLatitude(), location.getLongitude()));
                                if (distance_n < min_distance){

                                    min_distance = alarm.getlocation().getDistance(new Point(location.getLatitude(), location.getLongitude()));
                                    alarm_near = alarm;

                                }
                            }
                        }

                }

                distance = alarm_near.getlocation().getDistance(new Point(location.getLatitude(), location.getLongitude()));

                Log.e(TAG, "onLocationChangedNear: ID_ULTIMO: " + alarmSettings.getLast_alarm_id());
                Log.e(TAG, "onLocationChangedNear: '" + alarm_near.getName() + "' DISTANCIA: "+ distance);

                //VALIDAR SI EXISTE UNA ALARMA
                if(alarm_near.getId() != 0) {
                    Alarm alarm = alarm_near;

                    if (distance <= alarm.getRange()
                            && alarmSettings.getLast_alarm_id() != alarm.getId()
                            && alarm.getLastActiveInMinutes() > 10
                            ){

                        Log.e(TAG, "onLocationChangedDetected: ALARMA DETECTADA: '" + alarm.getName() + "' DISTANCIA: "+ distance);

                        alarmSettings.setLast_alarm_id(alarm.getId());
                        if (alarmDB.setAlarmSettings(alarmSettings)){
                            alarmDB.setLastActive(alarm);
                        }

                        intent_alarm = new Intent(getApplicationContext(), ShowAlarm.class);
                        intent_alarm.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent_alarm.putExtra("id",alarm.getId());
                        startActivity(intent_alarm);

                    }

                }

                //CALCULAR EL TIEMPO EN MILISEGUNDOS PARA DORMIR EL PROCESO
                int time_milise = 1;
                time_milise = calculateTime(distance,alarm_near.getId());

                Log.e(TAG, "onLocationChangedNotDetected: REVISAR DESPUES DE: " + (time_milise/1000) + " SEGUNDOS ");
                //Log.e(TAG, "onLocationChangedNotDetected: DURMIENDO......");


                //QUITAR EL LISTENER PARA NO ACTUALIZAR EL GPS
                //mLocationManager.removeUpdates(this);

                //DORMIR EL SERVICIO PARA AHORRAR BATERIA SI EL TIEMPO ES MAYOR A 5 SEGUNDOS
                //if (time_milise > 5000)
                //Thread.sleep(time_milise);


                //ACTIVAR EL GPS LISTENER PARA OBTENER LA NUEVA ACTUALIZACION
                mLocationManager.requestLocationUpdates(
                        GPS_PROVIDER, time_milise, LOCATION_DISTANCE,
                        mLocationListeners);

                //COLOCA LA ULTIMA UBICACION DEL GPS
                mLastLocation.set(location);

            } catch (java.lang.SecurityException ex) {
                Log.e(TAG, "onLocationChangedFAIL, ignore" + ex.getMessage());
            } catch (IllegalArgumentException ex) {
                Log.e(TAG, "onLocationChangedFAIL" + ex.getMessage());
            /*} catch (InterruptedException e) {
                Log.e(TAG, "onLocationChangedFAIL" + e.getMessage());*/
            }catch (Exception e){
                Log.e(TAG, "onLocationChangedFAIL" + e.getMessage());
            }
        }

        @Override
        public void onProviderDisabled(String provider)
        {
            boolean gpsEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (!gpsEnabled) {

                intent_request_gps = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                intent_request_gps.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent_request_gps);
            }
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider)
        {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras)
        {
            return;
            //Log.e(TAG, "onStatusChanged: " + provider);
        }
    }

    LocationListener mLocationListeners = new LocationListener(GPS_PROVIDER);


    @Override
    public IBinder onBind(Intent arg0)
    {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate()
    {

        alarmDB = new AlarmDB(this.getApplicationContext());
        Log.e(TAG, "onCreate");
        initializeLocationManager();
    }

    @Override
    public void onDestroy()
    {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {

                try {
                    mLocationManager.removeUpdates(mLocationListeners);
                } catch (Exception ex) {
                    Log.e(TAG, "fail to remove location listners, ignore" + ex.getMessage());
                }

        }
    }

    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
            boolean gpsEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (!gpsEnabled) {


                intent_request_gps = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                intent_request_gps.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent_request_gps);

            }
            else {
                try {

                    mLocationManager.requestLocationUpdates(
                            GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                            mLocationListeners);

                    Log.e(TAG, "LISTENING EVERY " + LOCATION_INTERVAL + " MILISEGUNDOS ");


                } catch (java.lang.SecurityException ex) {
                    Log.e(TAG, "fail to request location update, ignore" + ex.getMessage());
                } catch (IllegalArgumentException ex) {
                    Log.e(TAG, "gps provider does not exist " + ex.getMessage());
                }
            }
        }
    }


    /**
     * Retorna el tiempo en milisegundos para llegar a la distancia
     * Requiere el id para validar si no es la alarma por default
     * @param distance Double
     * @param alarm_id int
     * @return int
     */
    private int calculateTime(Double distance, int alarm_id){

        int time_milise = 0;
        int time = 0;
        
        if (distance <= 1500){
            time = 2;
        }
        else if (distance >= 10000 || alarm_id == 0){
            time = 30;
        }
        else {
            time = (int) Math.round((distance / 13)/30);
        }

        time_milise = (int) time * 1000;

        return time_milise;


    }


    private void requestEnableGPS(){

    }



}
