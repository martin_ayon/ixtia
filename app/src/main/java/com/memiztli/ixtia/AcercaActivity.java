package com.memiztli.ixtia;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class AcercaActivity extends AppCompatActivity implements View.OnClickListener {
    Button btn_acerca_ok;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acerca);

        btn_acerca_ok = (Button) findViewById(R.id.btn_acerca_ok);
        btn_acerca_ok.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_acerca_ok){
            this.finish();
        }
    }
}
