package com.memiztli.ixtia;

/**
 * Created by martin on 25/02/18.
 */

public class AlarmSettings {
    int id;
    int last_alarm_id;
    String last_alarm_active;

    public AlarmSettings(int id, int last_alarm_id, String last_alarm_active) {
        this.id = id;
        this.last_alarm_id = last_alarm_id;
        this.last_alarm_active = last_alarm_active;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AlarmSettings() {
        this.id = 0;
        this.last_alarm_id = 0;
        this.last_alarm_active = "";
    }

    public int getLast_alarm_id() {
        return last_alarm_id;
    }

    public void setLast_alarm_id(int last_alarm_id) {
        this.last_alarm_id = last_alarm_id;
    }

    public String getLast_alarm_active() {
        return last_alarm_active;
    }

    public void setLast_alarm_active(String last_alarm_active) {
        this.last_alarm_active = last_alarm_active;
    }
}
