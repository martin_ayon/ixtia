package com.memiztli.ixtia;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;

import java.util.Map;

import static android.location.LocationManager.GPS_PROVIDER;

public class AddAlarm extends AppCompatActivity implements GoogleMap.OnMapLongClickListener, OnMapReadyCallback, SeekBar.OnSeekBarChangeListener, View.OnClickListener {

    GoogleMap map;
    EditText et_name;
    TextView tv_range;
    SeekBar sbar_range;
    Marker marker = null;
    Circle circle = null;
    Switch switchActive;
    AlarmDB alarmDB;
    ShowcaseView showcaseView;
    Target t_txt_name, t_sbar_range, t_map, t_swich_active;
    boolean isFirstTime = true;
    int contador = 0;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 100;


    final int color_purple = Color.argb(50, 213, 0, 249);

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        isFirstTime = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getBoolean("isFirstAdd", true);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_alarm);

        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_add);


        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        mapFragment.getMapAsync(this);


        tv_range = (TextView) findViewById(R.id.tv_range_add);

        et_name = (EditText) findViewById(R.id.et_name_add);

        sbar_range = (SeekBar) findViewById(R.id.sbar_range);
        sbar_range.setOnSeekBarChangeListener(this);
        sbar_range.incrementSecondaryProgressBy(20);

        switchActive = (Switch) findViewById(R.id.sw_active_add);
        alarmDB = new AlarmDB(this);

        t_txt_name = new ViewTarget(R.id.et_name_add, this);
        t_swich_active = new ViewTarget(R.id.sw_active_add, this);
        t_sbar_range = new ViewTarget(R.id.sbar_range, this);
        t_map = new ViewTarget(R.id.map_add, this);


        if (isFirstTime) {

            showcaseView = new ShowcaseView.Builder(this)
                    .setTarget(Target.NONE)
                    .setOnClickListener(this)
                    .setContentTitle("Ixtia")
                    .setContentText("Agregar Alarma a Ixtia")
                    .setStyle(R.style.StyleShowCase)
                    .build();
            showcaseView.setButtonText("Siguiente");

        }


    }

    @Override
    public void onMapLongClick(LatLng latLng) {

        if (marker != null)
            marker.remove();

        marker = map.addMarker(new MarkerOptions().position(latLng).title("Localización"));

        if (circle != null) {
            circle.remove();
        }

        CircleOptions circleOptions = new CircleOptions();
        circleOptions.center(marker.getPosition());
        circleOptions.radius(sbar_range.getProgress() + Alarm.MIN_RANGE);
        circleOptions.strokeColor(getResources().getColor(R.color.colorPrimaryDark));
        circleOptions.fillColor(color_purple);
        circleOptions.strokeWidth(2);

        circle = map.addCircle(circleOptions);


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        this.map.setOnMapLongClickListener(this);

        double latitude = 20.6771119;
        double longitude = -103.3462624;

        LatLng current_location = new LatLng(latitude, longitude);

        map.moveCamera(CameraUpdateFactory.newLatLng(current_location));
        map.moveCamera(CameraUpdateFactory.zoomTo(12));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, MY_PERMISSIONS_REQUEST_LOCATION);

        }
        else {
            map.setMyLocationEnabled(true);
        }



    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        if (map != null) {

            progress = progress / 10;
            progress = progress * 10;

            tv_range.setText("Rango (" + (progress + Alarm.MIN_RANGE) + " M )");

            seekBar.setProgress(progress);
            if (marker != null) {


                CircleOptions circleOptions = new CircleOptions();
                circleOptions.center(marker.getPosition());
                circleOptions.radius(progress + Alarm.MIN_RANGE);
                circleOptions.strokeColor(getResources().getColor(R.color.colorPrimaryDark));
                circleOptions.fillColor(color_purple);
                circleOptions.strokeWidth(2);


                if (circle != null) {
                    circle.remove();
                }

                circle = map.addCircle(circleOptions);

            }
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }


    //METODO PARA CREAR EL MENU
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_alarm, menu);
        return true;
    }

    //METODO PARA EL LISTENER DEL MENU
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == R.id.action_save_alarm) {
            if (marker == null) {
                Snackbar snackbar = Snackbar
                        .make(this.et_name, "Manten presionado para seleccionar ubicacion", Snackbar.LENGTH_LONG);

                snackbar.show();
                return false;
            } else if (et_name.getText().toString().isEmpty()) {
                et_name.setHint("Escribe un nombre");
                et_name.setHintTextColor(ResourcesCompat.getColor(getResources(), R.color.colorAccent, null));
                return false;
            } else {

                AlarmDB alarmDB = new AlarmDB(et_name.getContext());

                String name = et_name.getText().toString().trim();
                double latitude = this.marker.getPosition().latitude;
                double longitude = this.marker.getPosition().longitude;
                boolean isActive = this.switchActive.isChecked();
                double range = this.sbar_range.getProgress() + Alarm.MIN_RANGE;

                Alarm alarm = new Alarm();
                alarm.setName(name);
                alarm.setLatitude(latitude);
                alarm.setLongitude(longitude);
                alarm.setActive(isActive);
                alarm.setRange(range);

                if (alarmDB.addAlarm(alarm)) {
                    this.setResult(Activity.RESULT_OK);
                    this.finish();
                } else {
                    this.setResult(Activity.RESULT_OK);
                    this.finish();
                }


            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (isFirstTime) {

            switch (contador) {
                case 0:
                    showcaseView.setShowcase(t_txt_name, true);
                    showcaseView.setContentTitle("Nombre de alarma");
                    showcaseView.setContentText("Escribe un nombre para la alarma");
                    break;
                case 1:
                    showcaseView.setShowcase(t_swich_active, true);
                    showcaseView.setContentTitle("Activar alarma");
                    showcaseView.setContentText("Activa o desactiva tu alarma");
                    break;
                case 2:
                    showcaseView.setShowcase(t_sbar_range, true);
                    showcaseView.setContentTitle("Rango de alarma");
                    showcaseView.setContentText("Rango a la redonda de la alarma en metros");
                    break;
                case 3:
                    showcaseView.setShowcase(t_map, true);
                    showcaseView.setContentTitle("Ubicaci\u00F3n");
                    showcaseView.setContentText("Menten presionado para agregar un marcador al mapa");
                    showcaseView.setButtonText("Cerrar");
                    break;
                case 4:
                    showcaseView.hide();
                    getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit().putBoolean("isFirstAdd", false).commit();
                    break;
            }

            contador++;

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                        return;
                    }
                    else
                        map.setMyLocationEnabled(true);

                } else {

                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, MY_PERMISSIONS_REQUEST_LOCATION);

                }
                return;
            }

        }
    }
}
